Triage
======

Este modulo sirve para provisionar una instancia 100% funcional de Triage para lo cual se instalan los siguientes componentes:

* Java 7 (openjdk)
* Nginx
* PostgreSQL

Para que este modulo funcione es necesario generar el jar de traige a partir de los fuentes publicados aquí (https://github.com/nestor-m/triage) y ubicarlo en la carpeta _files_.

una vez completado la ejecución del módulo la aplicación queda corriendo en http://localhost y es accesible con credenciales u:lius y p:triage.
