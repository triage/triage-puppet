class triage::runtime {
  
  class { 'java':
    package => 'openjdk-7-jre-headless'
  }

  class { 'nginx': }
  
  nginx::resource::upstream { 'triage_app':
    members => ['localhost:8080'],
  }->
  file { '/etc/nginx/sites-available/triage.com.conf':
    ensure => present,
    content => template("triage/triage.com.conf.erb")
  }->
  file { '/etc/nginx/sites-enabled/triage.com.conf':
   ensure => 'link',
   target => '/etc/nginx/sites-available/triage.com.conf',
  }

}