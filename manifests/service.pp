class triage::service {

  user { 'triage':
    ensure => 'present',
  }

  file { ['/var/www/', '/var/www/triage']:
    ensure => 'directory',
    mode => "a+w"
  }

  file { '/var/www/triage/triage.jar':
    ensure => 'present',
    source => "puppet:///modules/triage/triage.jar",
    require => File['/var/www/', '/var/www/triage'],
    before => Service['triage']
  }

  file { '/var/www/triage/triage-config.groovy':
    ensure => 'present',
    source => "puppet:///modules/triage/triage-config.groovy",
    require => File['/var/www/', '/var/www/triage'],
    before => Service['triage']
  }

  file { '/etc/init/triage.conf':
    ensure => present,
    content => template("triage/triage.conf.erb")
  }

  service { 'triage':
    ensure => running,
    provider => 'upstart',
    require => File['/etc/init/triage.conf']
  }

}