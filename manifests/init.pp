class triage {

	class apt_update {
  	exec { 'do_apt_update':
    	command     => '/usr/bin/apt-get update',
    	timeout     => 300,
  	}
	}

	stage { 'init': before  => Stage['main'] }

	class { 'apt_update': 
  	stage => init
	}

  class { 'triage::database': }
  
  class { 'triage::runtime': }
  
  class { 'triage::service': 
    require => [Class['triage::runtime'], Class['triage::database']]
  }

}
